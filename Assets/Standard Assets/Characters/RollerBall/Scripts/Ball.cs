using System;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Ball
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private float m_MovePower = 5; // The force added to the ball to move it.
        [SerializeField] private bool m_UseTorque = false; // Whether or not to use torque to move the ball.
        [SerializeField] private float m_MaxAngularVelocity = 0; // The maximum velocity the ball can rotate at.
        [SerializeField] private float m_JumpPower = 2; // The force added to the ball when it jumps.

        private const float k_GroundRayLength = 2f; // The length of the ray to check if the ball is grounded.
        private Rigidbody m_Rigidbody;


        private void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
        }


        public void Move(Vector3 moveDirection, bool jump)
        {
            // If on the ground and jump is pressed...
            if (Physics.Raycast(transform.position, -Vector3.up, k_GroundRayLength)) 
			{
				if (!jump) {
					// Otherwise add force in the move direction.
					m_Rigidbody.AddForce(moveDirection*m_MovePower);
				} else {
					// ... add force in upwards.
					m_Rigidbody.AddForce(Vector3.up*m_JumpPower, ForceMode.Impulse);				
				}
			}
        }
		
		public float maxSpeed = 10f;//Replace with your max speed
		void FixedUpdate()
		{
			if(m_Rigidbody.velocity.magnitude > maxSpeed)
			{
				m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * maxSpeed;
			}
		}
    }
	
}
