﻿using UnityEngine;
using System.Collections;
using System;

//[RequireComponent(typeof(CharacterController))]

public class Chaser : MonoBehaviour {
	
	public float speed = 20.0f;
	public float minDist = 1f;
	public Transform target;

	private float timeCreated;
	private float timePassed;
	
	// Use this for initialization
	void Start () 
	{
		
		timeCreated = Time.time;
		// if no target specified, assume the player
		if (target == null) {

			if (GameObject.FindWithTag ("Player")!=null)
			{
				target = GameObject.FindWithTag ("Player").GetComponent<Transform>();
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{		
		if (target == null)
			return;

		// face the target
		transform.LookAt(target.position - new Vector3(1, 0, 1));
		
		timePassed=Time.time-timeCreated;
		//so long as the chaser is farther away than the minimum distance, move towards it at rate speed.
		
		//get the distance between the chaser and the target
		float distance = Vector3.Distance(transform.position,target.position - new Vector3(1, 0, 1));
		if(distance > minDist)
			transform.position += transform.forward * speed * Time.deltaTime;
	}

	// Set the target of the chaser
	public void SetTarget(Transform newTarget)
	{
		target = newTarget;
	}

}
