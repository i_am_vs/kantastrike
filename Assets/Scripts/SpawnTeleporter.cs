﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTeleporter : MonoBehaviour {

	public float range = 5f;
	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		Vector3 position = new Vector3(Random.Range(-range, range), 10, Random.Range(-range, range));
		transform.position = position;
	}
}
